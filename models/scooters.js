var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    id: {type: Number, required: true},
    latitude: {type: Number, required: true},
    longitude: {type: Number, required: true},
    isReserved: {type: Boolean, required: true}
});

module.exports = mongoose.model('Scooter', schema);