const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const insertRoute = require('./routes/insert');
const availableRoute = require('./routes/available');
const reserveRoute = require('./routes/reserve');
const unreserveRoute = require('./routes/unreserve');

const app = express();
mongoose.connect('mongodb://localhost:27017/scooter');

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json());

app.use('/api/v1/scooters/insert', insertRoute);
app.use('/api/v1/scooters/available', availableRoute);
app.use('/api/v1/scooters/reserve', reserveRoute);
app.use('/api/v1/scooters/unreserve', unreserveRoute);

const port = process.env.PORT || 3000;
app.listen(port);

console.log(`Address book server running on localhost:${port}`);

module.exports = app