var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var Scooter = require('../models/scooters');

router.post('/', function(req, res) {
    var scooter = new Scooter({
        id: req.body.id,
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        isReserved: req.body.isReserved,
        distance: req.body.distance
    });
    scooter.save((err,result) => {
        if(err) {
            return res.status(500).json({
                title: 'An error occurred',
                error: err
            });
        }
        res.status(201).json({
            message: 'New Scooter Inserted',
            obj: result
        });
    });
});

module.exports = router