var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var Scooter = require('../models/scooters');

router.post('/', (req, res) => {
    Scooter.find({id: req.query.id}, (err, scooter) => {
        if (err) {
            return res.status(500).json({
                message: 'A scooter with the given id does not exist',
                error: err
            });
        }
        if(scooter[0].isReserved) {
            return res.status(401).json({
                title: 'The scooter with the given id is already reserved',
                error: err
            });
        }
        scooter[0].isReserved = true;
        scooter[0].save(function (err, result) {
            if (err) {
                return res.status(500).json({
                    title: 'An error occurred',
                    error: err
                });
            }
            res.status(200).json({
                message: 'Reserved',
                obj: result
            });
        });
    });
});

module.exports = router;