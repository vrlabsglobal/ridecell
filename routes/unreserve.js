var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var Scooter = require('../models/scooters');

function findDistance(lat1, lat2, lon1, lon2) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    }
    else {
        var radlat1 = Math.PI * lat1/180;
        var radlat2 = Math.PI * lat2/180;
        var theta = lon1-lon2;
        // console.log(theta)
        var radtheta = Math.PI * theta/180;
        // console.log(radtheta);
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        // console.log(dist);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        // console.log(radlat2);
        dist = dist * 180/Math.PI;
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344 * 1000
        return dist;
    }
}

router.post('/', (req, res) => {
    Scooter.find({id: req.query.id}, (err, scooter) => {
        if (err) {
            return res.status(500).json({
                message: 'A scooter with the given id does not exist',
                error: err
            });
        }

        if(req.query.latitude == undefined || req.query.longitude == undefined) {
            return res.status(500).json({
                message: 'Kindly enter all the required query parameters',
            });
        }

        if(!scooter[0].isReserved) {
            return res.status(401).json({
                title: 'The scooter with the given id is already unreserved',
                error: err
            });
        }
        
        dist = findDistance(req.query.latitude, scooter[0].latitude, req.query.longitude, scooter[0].longitude);
        scooter[0].isReserved = false;
        scooter[0].latitude = req.query.latitude;
        scooter[0].longitude = req.query.longitude;
        scooter[0].save(function (err, result) {
            if (err) {
                return res.status(500).json({
                    title: 'An error occurred',
                    error: err
                });
            }
            res.status(200).json({
                message: 'End of Reservation',
                distance: dist,
                price: dist*0.5,
                obj: result
            });
        });
    });
});

module.exports = router;