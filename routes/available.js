var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var Scooter = require('../models/scooters');

function findDistance(lat1, lat2, lon1, lon2) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    }
    else {
        var radlat1 = Math.PI * lat1/180;
        var radlat2 = Math.PI * lat2/180;
        var theta = lon1-lon2;
        // console.log(theta)
        var radtheta = Math.PI * theta/180;
        // console.log(radtheta);
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        // console.log(dist);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        // console.log(radlat2);
        dist = dist * 180/Math.PI;
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344 * 1000
        return dist;
    }
}

router.get('/', (req, res) => {
    Scooter.find({isReserved: false}, (err, scooter) => {
        if (err) {
            return res.status(500).json({
                title: 'An error occurred',
                error: err
            });
        }
        var obj = [];
        o_latitude = req.query.latitude;
        o_longitude = req.query.longitude;
        o_radius = req.query.radius;
        if(o_latitude == undefined || o_longitude == undefined || o_radius == undefined) {
            return res.status(500).json({
                message: 'Kindly enter all the required query parameters',
            });
        }
        for(var i=0; i<scooter.length; i++) {
            dist = findDistance(o_latitude, scooter[i].latitude, o_longitude, scooter[i].longitude);
            // console.log(dist < o_radius);
            if(dist < o_radius) {
                obj.push({
                    "id":scooter[i].id,
                    "lat":scooter[i].latitude,
                    "lng":scooter[i].longitude
                });
            }
        }
        return res.status(200).json({
            "locations": obj
        });
    });
});

module.exports = router;