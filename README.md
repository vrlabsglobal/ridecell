I have implemented the challenge in Node/Express stack as it required less time to implement and I could complete the challenge in approx 4 hours

After downloading the code,

1) Run npm install  --> to install all the dependencies
2) Run npm run test ---> to run a few automation tests that I came up with
3) Install mongoDb and make sure it's running on localhost:27017, by typing the command mongod in the cmd.
3) Run node server.js  ---> to run the server on localhost:3000

To enter the parking position of a new scooter use the end point:
POST /api/v1/scooters/insert/    ---->  the body should be of the form {
	"id":8,
	"latitude": 40.48059,
	"longitude" : -74.444359,
	"isReserved": false
}

To get all available parking spots within the radius use the end point:
GET api/v1/scooters/available?latitude=40.337314&Longitude=-74.064974&radius=2000

To Reserve a scooter use the end point:
POST /api/v1/scooters/reserve?id=1

To end the reservation of an already reserved scooter use the endpoint:
POST api/v1/scooters/unreserve?id=2&latitude=40.310098&longitude=-74.250286

Among the things that I have implemented nothing was new to me as such, but I got blocked on trying to implement the mocking of a payment as I couldn't quiet understand
what does that actually mean.

In terms of improving my current solution,

Currently I am using a for loop to loop through all the available scooters to find the nearest one's, that could be improved to using some advanced filtering methods.
I would have incorporated a thrid party payment gateway like paypal to process the payments after the scooter is unreserved.

