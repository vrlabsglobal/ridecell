var supertest = require("supertest");
var should = require("should");
var expect = require("chai").expect;

var server = supertest.agent("http://localhost:3000/api/v1/scooters");

describe('Automation Scripts', function() {

    it('should return all available scooters nearby', function(done) {
        server.get("/available?latitude=40.337314&longitude=-74.064974&radius=2000").expect("content-type",/json/)
        .expect(200)
        .end(function(err,res) {
            res.status.should.equal(200);
            expect(res.body).to.have.property('locations');
            done();
        })
    });

    it('should give error for reserve a scooter', function(done) {
        server.post("/reserve?id=4").expect("content-type",/json/)
        .expect(200)
        .end(function(err,res) {
            res.status.should.equal(401);
            expect(res.body.title).to.equal("The scooter with the given id is already reserved");
            done();
        });
    });

    it('should reserve a scooter', function(done) {
        server.post("/reserve?id=5").expect("content-type",/json/)
        .expect(200)
        .end(function(err,res) {
            res.status.should.equal(200);
            expect(res.body.obj.isReserved).to.be.true;
            done();
        });
    });

    it('should unreserve a scooter', function(done) {
        server.post("/unreserve?id=5&latitude=40.310098&longitude=-74.250286").expect("content-type",/json/)
        .expect(200)
        .end(function(err,res) {
            res.status.should.equal(200);
            expect(res.body.obj.isReserved).to.be.false;
            done();
        });
    });
})